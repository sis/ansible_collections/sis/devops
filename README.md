# Ansible Collection - sis.devops

This ansible collection consists of roles and playbooks dedicated to
devops.

The main role is [gitlab_runner](roles/gitlab_runner) which deploys
[gitlab-runner](https://docs.gitlab.com/runner/) with a podman 🦭 or
docker 🐋 backend.

## Installation

```sh
ansible-galaxy collection install git@gitlab.ethz.ch:sis/ansible_collections/sis/devops.git
```

This will clone the project and install it into
`~/.ansible/collections/ansible_collections/sis/devops`.


## Usage

Example: Deploy gitlab-runner:

Create an inventory file for your target host:

File: `vcosmo-ci.yml`

```yml
all:
  hosts:
    vcosmo-ci.ethz.ch:
      remote_user: root
      gitlab_runner_container_engine: podman
      gitlab_runner_container_registry: false
      gitlab_runner_registrations:
        - name: "vcosmo-ci.ethz.ch"
          registration_token: "##################"
          container_image: "library/python"
          gitlab_url: "https://cosmo-gitlab.phys.ethz.ch"
```

Then run (need an entry in `.ssh/config` named `vcosmo-ci.ethz.ch`):

```sh
ansible-playbook -i vcosmo-ci.yml sis.devops.gitlab_runner_podman
```

## Develop


### Installation

Instead of installing the repository using `ansible-galaxy`, you can
clone the repo into the same directory path:

```sh
mkdir -p ~/.ansible/collections/ansible_collections/sis/
cd ~/.ansible/collections/ansible_collections/sis/
git clone git@gitlab.ethz.ch:sis/ansible_collections/sis/devops.git
```

Alternatively, clone to some different path, then create or edit
`~/.ansible.cfg` and add the following:

```ini
[defaults]

...
collections_paths = <your custom path>
...
```

⚠: In order to be able to run tests and enable ansible to discover the
collection, the repository has to be placed into a folder hierarchy
like this:

```
<your custom path>/ansible_roles/sis/devops
```

* `sis` is the namespace
* `devops` the repository-name


### Write tests

To test your role, you need to write an ansible playbook and place
them into [tests/integration/targets](tests/integration/targets) (for
integration tests) See the [Ansible Community General
Collection](https://github.com/ansible-collections/community.general)
for examples.


### Run your tests locally

This requires docker 🐋.
It will run the test-playbook using the
`ansible/default-test-container`.
Run the tests from inside the project folder like this:

```sh
ansible-test integration --docker
```
