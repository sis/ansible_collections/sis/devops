---
# See: https://docs.gitlab.com/runner/install/linux-repository.html

- name: Apt based installation
  when: ansible_pkg_mgr == "apt"
  block:
    - name: Add gitlab-runner gpg key
      ansible.builtin.apt_key:
        url: https://packages.gitlab.com/runner/gitlab-runner/gpgkey
    - name: Set os mapping
      ansible.builtin.set_fact:
        distro: "{{ ansible_distribution | lower }}"
        release: "{{ ansible_distribution_release }}"
    - name: Add gitlab-runner repository
      ansible.builtin.apt_repository:
        repo:
          "{{ item }} https://packages.gitlab.com/runner/gitlab-runner/{{ distro }}/ {{ release }} main"
      loop: ["deb", "deb-src"]
    - name: Install gitlab-runner
      ansible.builtin.apt:
        name: gitlab-runner
      # In the integration tests, we get for some reson an exit code 1
      # here, but gitlab-runner still seems to be
      # functional. Therefore we ignore this error when
      # is_integration_test is true:
      register: out_apt
      failed_when: |-
        {{ out_apt.failed and not (is_integration_test | default(false)) }}

- name: Rpm based installation
  when: ansible_pkg_mgr == "yum" or ansible_pkg_mgr == "dnf"
  block:
    - name: Add gitlab-runner repository
      ansible.builtin.yum_repository:
        name: "{{ item.name }}"
        baseurl: "https://packages.gitlab.com/runner/gitlab-runner/el/{{ ansible_distribution_release }}/{{ item.subrepo }}"
        gpgkey:
          - https://packages.gitlab.com/runner/gitlab-runner/gpgkey
          - https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-4C80FB51394521E9.pub.gpg
      loop:
        - name: runner_gitlab-runner
          subrepo: "$basearch"
        - name: runner_gitlab-runner-source
          subrepo: "SRPMS"
    - name: Install gitlab-runner
      ansible.builtin.yum:
        name: gitlab-runner

- name: Create /etc/systemd/system/gitlab-runner.service.d
  when: gitlab_runner_proxy_env
  ansible.builtin.file:
    name: /etc/systemd/system/gitlab-runner.service.d
    state: directory
    owner: gitlab-runner
    mode: 0755

- name: Create proxy config for gitlab-runner
  when: gitlab_runner_proxy_env
  ansible.builtin.copy:
    dest: /etc/systemd/system/gitlab-runner.service.d/http-proxy.conf
    owner: gitlab-runner
    mode: 0644
    content: |
      [Service]
      Environment="{{ gitlab_runner_proxy_env.keys()
        | zip(gitlab_runner_proxy_env.values())
        | map("join", '=')
        | join('"\nEnvironment="')
      }}"

- name: Define pkg_cmd for automatic gitlab-runner update
  ansible.builtin.set_fact:
    pkg_cmd:
      apt: apt-get
      yum: yum
      dnf: dnf
- name: Automatically update gitlab-runner
  ansible.builtin.cron:
    name: "Upgrade gitlab-runner"
    hour: "03"
    minute: "00"
    day: "*"
    user: root
    job: |-
      {{ pkg_cmd[ansible_pkg_mgr] }} update && {{ pkg_cmd[ansible_pkg_mgr] }} install -y gitlab-runner
    cron_file: "gitlab-runner-upgrade"

- name: Create /var/lib/gitlab-runner
  # This is needed by gitlab-runner but not created automatically in
  # all cases
  ansible.builtin.file:
    name: /var/lib/gitlab-runner
    state: directory
    owner: gitlab-runner
    mode: 0755

- name: Get a list of already registered runners
  tags: register
  ansible.builtin.command:
    grep -Po '^ *name = "\K[^"]*' /etc/gitlab-runner/config.toml
  changed_when: false
  register: out_gitlab_runner_register
  # grep exits with exit code 1 if there is no match:
  failed_when: out_gitlab_runner_register.rc >= 2

- name: Define container engine specific options for registration
  # See
  # https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands-beta
  tags: register
  ansible.builtin.set_fact:
    register_opts:
      docker: []
      podman: ["--docker-host", "unix:///run/podman/podman.sock"]
    register_opts_proxy: |-
      {{ gitlab_runner_proxy_env
         | ternary ([
           '--pre-clone-script',
           '"git config --global http.proxy $HTTP_PROXY; git config --global https.proxy $HTTPS_PROXY"'
           ], [])
      }}
- name: Build proxy register opts
  tags: register
  when: gitlab_runner_proxy_env
  ansible.builtin.set_fact:
    register_opts_proxy: |-
      {{ register_opts_proxy + ['--env', item.key + '=' + item.value] }}
  loop: "{{ gitlab_runner_proxy_env | dict2items }}"

- name: Register runners
  tags: register
  loop: "{{ gitlab_runner_registrations }}"
  when: item.name not in out_gitlab_runner_register.stdout.split()
  # Passing --docker-pull-policy twice, to achieve
  # pull-policy = ["always", "if-not-present"]
  # This should prevent running into the docker.io pull limit:
  ansible.builtin.command: |-
    gitlab-runner register \
      --non-interactive \
      --url "{{ item.gitlab_url }}" \
      --registration-token "{{ item.registration_token }}" \
      --locked=false \
      --name "{{ item.name }}" \
      --executor {{ item.executor | default('docker') }} \
      --docker-image "{{ item.container_image }}" \
      --tag-list "{{ item.tags | default('') }}" \
      --docker-pull-policy "always" \
      --docker-pull-policy "if-not-present" \
      {{ (register_opts[gitlab_runner_container_engine] +
         register_opts_proxy) | join(' ') }}

- name: Verify runner
  tags: register
  ansible.builtin.command: "gitlab-runner verify"
  changed_when: false

- name: Add cron job for docker / podman prune
  ansible.builtin.cron:
    name: "{{ gitlab_runner_container_engine }} prune"
    hour: "02"
    minute: "00"
    day: "*"
    user: root
    job: |-
      {{ gitlab_runner_container_engine }} image prune -a -f ; {{ gitlab_runner_container_engine }} volume prune -f
    cron_file: "{{ gitlab_runner_container_engine }}-prune"

- name: Set up a container registry caching mirror
  when: gitlab_runner_container_registry
  ansible.builtin.include_role:
    name: sis.devops.container_registry
  vars:
    container_registry_engine: "{{ gitlab_runner_container_engine }}"
    container_registry_user: gitlab-runner
